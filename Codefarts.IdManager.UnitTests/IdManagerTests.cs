﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Codefarts.WowTracks.UnitTests
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.IdManager;

    // using Codefarts.WowTracks.DataAcquisitionLib;

    [TestClass]
    public class IdManagerTests
    {
        [TestMethod]
        public void TestSequence()
        {
            var manager = new IdManager(5);
            for (var i = 0; i < manager.BufferCount + 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public void TestBrokenSequence()
        {
            var manager = new IdManager(5);
            manager.RegisterId(1);
            Assert.AreEqual(0, manager.NewId());
            Assert.AreEqual(2, manager.NewId());
            for (var i = 3; i < manager.BufferCount + 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public void TestForwardSequence()
        {
            var manager = new IdManager(5);
            manager.RegisterId(0);
            manager.RegisterId(1);
            manager.RegisterId(2);
            Assert.AreEqual(3, manager.NewId());
            Assert.AreEqual(4, manager.NewId());
            for (var i = 5; i < manager.BufferCount + 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public void TestBackwardSequence()
        {
            var manager = new IdManager(5);
            manager.RegisterId(2);
            manager.RegisterId(1);
            manager.RegisterId(0);
            Assert.AreEqual(3, manager.NewId());
            Assert.AreEqual(4, manager.NewId());
            for (var i = 5; i < manager.BufferCount + 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public async Task TestLargeNumbersRegistration()
        {
            // register a list of id's from 0 to 1000
            var list = new List<int>();
            for (int i = 0; i < 1000; i++)
            {
                list.Add(i);
            }

            var manager = new IdManager(1000);
            manager.RegisterId(list);

            var taskCount = 10000;
            var taskList = new Task[taskCount];
            var idValue = 0;
            for (int i = 0; i < taskList.Length; i++)
            {
                manager.RegisterId(idValue++);
            }

            Assert.AreEqual(taskCount, manager.NewId());
        }

        [TestMethod]
        public async Task TestAsyncRegistration()
        {
            // register a list of id's from 0 to 1000
            var list = new List<int>();
            for (int i = 0; i < 1000; i++)
            {
                list.Add(i);
            }

            var manager = new IdManager(1000);
            manager.RegisterId(list);

            var taskCount = 10000;
            var taskList = new Task[taskCount];
            var idValue = -1;
            for (int i = 0; i < taskList.Length; i++)
            {
                taskList[i] = Task.Factory.StartNew(() => manager.RegisterId(Interlocked.Increment(ref idValue)));
            }

            Task.WaitAll(taskList);

            Assert.AreEqual(taskCount - 1, idValue);
            Assert.AreEqual(taskCount, manager.NewId());
        }

        [TestMethod]
        public async Task TestAsyncRandomRegistration()
        {
            // register a list of id's from 0 to 1000
            var list = new List<int>();
            for (int i = 0; i < 1000; i++)
            {
                list.Add(i);
            }

            // randomize the order of the id's in the list
            var random = new Random((int)DateTime.Now.Ticks);
            var randomizedList = from item in list
                                 orderby random.Next()
                                 select item;

            var manager = new IdManager(1000);
            manager.RegisterId(randomizedList);

            var taskCount = 10000;
            var taskList = new Task[taskCount];
            var idValue = -1;
            for (int i = 0; i < taskList.Length; i++)
            {
                taskList[i] = Task.Factory.StartNew(() => manager.RegisterId(Interlocked.Increment(ref idValue)));
            }

            Task.WaitAll(taskList);

            Assert.AreEqual(taskCount - 1, idValue);
            Assert.AreEqual(taskCount, manager.NewId());
        }
    }
}
