﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.WowTracks.UnitTests
{
    using System.Threading.Tasks;

    using Codefarts.IdManager;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class IdManagerSequenceTests
    {
        [TestMethod]
        public void TestSequence()
        {
            var manager = new IdManagerSequences();
            for (var i = 0; i < 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public void TestBrokenSequence()
        {
            var manager = new IdManagerSequences();
            manager.RegisterId(1);
            Assert.AreEqual(0, manager.NewId());
            Assert.AreEqual(2, manager.NewId());
            for (var i = 3; i < 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public void TestForwardSequence()
        {
            var manager = new IdManagerSequences();
            manager.RegisterId(0);
            manager.RegisterId(1);
            manager.RegisterId(2);
            Assert.AreEqual(3, manager.NewId());
            Assert.AreEqual(4, manager.NewId());
            for (var i = 5; i < 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public void TestBackwardSequence()
        {
            var manager = new IdManagerSequences();
            manager.RegisterId(2);
            manager.RegisterId(1);
            manager.RegisterId(0);
            Assert.AreEqual(3, manager.NewId());
            Assert.AreEqual(4, manager.NewId());
            for (var i = 5; i < 10; i++)
            {
                Assert.AreEqual(i, manager.NewId());
            }
        }

        [TestMethod]
        public async Task TestAsyncRegistration()
        {
            var manager = new IdManagerSequences();

            var taskList = new Task[10];
            var idValue = 0;
            for (var i = 0; i < taskList.Length; i++)
            {
                taskList[i] = Task.Factory.StartNew(() => manager.RegisterId(idValue++));
            }

            Task.WaitAll(taskList);

            Assert.AreEqual(10, manager.NewId());
        }
    }
}