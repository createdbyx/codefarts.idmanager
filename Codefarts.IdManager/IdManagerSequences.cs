﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.IdManager
{
    using System;
    using System.Collections.Generic;

    [Obsolete("Currently experimental do not use!", true)]
    public class IdManagerSequences
    {
        private struct Sequence
        {
            private int min;

            public int Min
            {
                get
                {
                    return this.min;
                }

                set
                {
                    if (value > this.Max)
                    {
                        throw new ArgumentOutOfRangeException("value", "Min property can not be greater then Max.");
                    }

                    this.min = value;
                }
            }

            private int max;

            public int Max
            {
                get
                {
                    return this.max;
                }

                set
                {
                    if (value < this.Min)
                    {
                        throw new ArgumentOutOfRangeException("value", "Max property can not be less then Min.");
                    }

                    this.max = value;
                }
            }

            public Sequence(int min, int max)
            {
                this.min = 0;
                this.max = int.MaxValue;
                this.Min = min;
                this.Max = max;
            }
        }

        private List<Sequence> sequenceList = new List<Sequence>(new[] { new Sequence(0, int.MaxValue), new Sequence(int.MinValue, -1) });

        // private SemaphoreSlim semaphore = new SemaphoreSlim(1);
        object obj = new object();

        //public ReadOnlyCollection<int> RegisteredIds
        //{
        //    get
        //    {
        //        return new ReadOnlyCollection<int>(this.idList);
        //    }
        //}

        public void RegisterId(int id)
        {
            //  this.semaphore.Wait();

            lock (this.obj)
            {
                if (this.sequenceList.Count == 0)
                {
                    throw new ArgumentOutOfRangeException("id", "There are no more available id values.");
                }

                var sequenceIndex = 0;
                while (sequenceIndex < this.sequenceList.Count)
                {
                    // get the sequence from the list
                    var s = this.sequenceList[sequenceIndex];

                    // if id is outside the bounds of the sequence then continue onto the next sequence
                    if (id < s.Min && id > s.Max)
                    {
                        sequenceIndex++;
                        continue;
                    }

                    // check if id matches the start of the sequence
                    if (id == s.Min)
                    {
                        // check if it's a sequence of one and if it is just remove it and exit
                        if (s.Min == s.Max)
                        {
                            this.RemoveSequence(sequenceIndex);
                            break;
                        }

                        // move the left side of the sequence to the right and exit
                        this.sequenceList[sequenceIndex] = new Sequence(s.Min + 1, s.Max);
                        break;
                    }

                    // check if id matches the end of the sequence
                    if (id == s.Max)
                    {
                        // check if it's a sequence of one and if it is just remove it and exit
                        if (s.Min == s.Max)
                        {
                            this.RemoveSequence(sequenceIndex);
                            break;
                        }

                        // move the right side of the sequence to the left and exit
                        this.sequenceList[sequenceIndex] = new Sequence(s.Min, s.Max - 1);
                        break;
                    }

                    // the id must be somewhere in between the sequence so split the sequence
                    this.SplitSequence(id, sequenceIndex, s);

                    // we can now exit
                    break;
                }
            }

            //    this.semaphore.Release();
        }

        private void SplitSequence(int id, int sequenceIndex, Sequence s)
        {
            //   this.semaphore.Wait();

            // lock (this.obj)
            // {
            this.sequenceList.RemoveAt(sequenceIndex);
            this.sequenceList.Insert(sequenceIndex, new Sequence(id + 1, s.Max));
            this.sequenceList.Insert(sequenceIndex, new Sequence(s.Min, id - 1));
            // }

            //   this.semaphore.Release();
        }

        private void RemoveSequence(int sequenceIndex)
        {
            // this.semaphore.Wait();

            // lock (this.obj)
            //  {
            this.sequenceList.RemoveAt(sequenceIndex);
            //  }

            //   this.semaphore.Release();
        }

        public void Reset()
        {
            // this.semaphore.Wait();

            lock (this.obj)
            {
                this.sequenceList.Clear();
                this.sequenceList.Add(new Sequence(0, int.MaxValue));
                this.sequenceList.Add(new Sequence(int.MinValue, -1));
            }

            //   this.semaphore.Release();
        }

        public void RegisterId(IEnumerable<int> ids)
        {
            foreach (var id in ids)
            {
                this.RegisterId(id);
            }
        }

        public int NewId()
        {
            // this.semaphore.Wait();

            Sequence first;
            lock (this.obj)
            {
                if (this.sequenceList.Count == 0)
                {
                    throw new ArgumentOutOfRangeException("id", "There are no more available id values.");
                }

                // get first sequence
                first = this.sequenceList[0];

                // check if it's a sequence of one and if it is just remove it and return the left part (IE: min)
                if (first.Min == first.Max)
                {
                    this.sequenceList.RemoveAt(0);
                    return first.Min;
                }

                // update sequence
                this.sequenceList[0] = new Sequence(first.Min + 1, first.Max);
            }

            // return the min value but also move the left side of the sequence to the right 
            // this.semaphore.Release();
            return first.Min++;
        }
    }
}